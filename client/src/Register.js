import React, { Component } from 'react'
import { Container, Form, Button, Segment, Header, Message } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { humanize } from './util/string';
import axios from 'axios';

class Register extends Component {
  state = { submitting: false, errors: {} }

  handleChange = (e, { name, value }) => this.setState({ [name]: value })

  handleSubmit = () => {
    this.setState({ errors: {}, submitting: true });

    axios.post('/users', { user: this.state })
      .then(response => {
        this.props.history.push('/login');
      })
      .catch(error => {
        if (error.response) {
          const response = error.response;
          this.setState({ errors: response.data.errors });
        } else {
          // Something went wrong generic error!
          console.log('Something went wrong!');
        }

        this.setState({ submitting: false });
      });
  }

  hasErrors = () => {
    return Object.keys(this.state.errors).length > 0
  }

  hasErrorFor = fieldName => {
    const keys = Object.keys(this.state.errors);

    return keys.indexOf(fieldName) >= 0;
  }

  errorList = () => {
    const { errors } = this.state;

    const keys = Object.keys(errors);
    let list = [];

    keys.forEach(key => {
      list.push(`${humanize(key)} ${errors[key][0]}`)
    });

    return list;
  }

  render () {
    const { submitting } = this.state

    return(
      <Segment padded="very" vertical>
        <Container text>
          <Header as="h1">Register new user</Header>

          <Form onSubmit={this.handleSubmit} loading={submitting} error={this.hasErrors()}>

            <Message error>
              <Message.Header>Oops! Seems like your form has some errors:</Message.Header>
              <Message.List>
                {this.errorList().map(error => <Message.Item key="error">{error}</Message.Item>)}
              </Message.List>
            </Message>

            <Form.Field>
              <Form.Input
                error={this.hasErrorFor('full_name')}
                label="Full name"
                name="full_name"
                placeholder="John Doe"
                onChange={this.handleChange} />
            </Form.Field>

            <Form.Field>
              <Form.Input
                error={this.hasErrorFor('email')}
                label="Email"
                name="email"
                type="email"
                placeholder="john@gmail.com"
                onChange={this.handleChange} />
            </Form.Field>

            <Form.Field>
              <Form.Input
                error={this.hasErrorFor('password')}
                label="Password"
                name="password"
                type="password"
                onChange={this.handleChange} />
            </Form.Field>

            <Form.Group>
              <Form.Button primary size="huge">Register</Form.Button>
              <Button as={Link} to="/" size="huge">Cancel</Button>
            </Form.Group>
          </Form>
      </Container>
    </Segment>
    );
  }
}

export default Register
