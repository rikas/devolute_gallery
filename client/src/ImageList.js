import React, { Component } from 'react';
import { Container, Button, Segment, Icon, Header } from 'semantic-ui-react';

class ImageList extends Component {
  render () {
    return <Container text textAlign='center'>
      <h1>Uploaded images</h1>
      <Segment placeholder>
          <Header icon>
            <Icon name='image file outline' />
            You have no uploaded images yet.
          </Header>
          <Button primary>Upload image</Button>
        </Segment>
    </Container>
  }
}

export default ImageList
