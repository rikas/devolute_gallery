export const capitalize = (string) => {
  return `${string.charAt(0).toUpperCase()}${string.slice(1).toLowerCase()}`;
}

export const humanize = (string) => {
  let parts = string.split('_');

  parts = parts.map(part => capitalize(part));

  return parts.join(' ');
}

export default { capitalize, humanize }
