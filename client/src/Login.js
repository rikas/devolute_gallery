import React, { Component } from 'react';
import { Container, Form, Message, Header, Button, Segment } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import axios from 'axios';

class NotFound extends Component {
  state = { submitting: false, errors: {} }

  handleChange = (e, { name, value }) => this.setState({ [name]: value })

  handleSubmit = () => {
    this.setState({ errors: {}, submitting: true });

    axios.post('/users/sign_in', { user: this.state })
      .then(response => {
        this.props.history.push('/images')
      })
      .catch(error => {
        if (error.response) {
          const response = error.response;
          this.setState({ errors: response.data.errors });
        } else {
          // Something went wrong generic error!
          console.log('Something went wrong!');
        }

        this.setState({ submitting: false });
      });
  }

  hasErrors = () => {
    return Object.keys(this.state.errors).length > 0;
  }

  render() {
    const { submitting } = this.state;

    return(
      <Segment padded="very" vertical>
        <Container text>
          <Header as="h1">Login</Header>

          <Form onSubmit={this.handleSubmit} loading={submitting} error={this.hasErrors()}>
            <Message error>
              Invalid email or password!
            </Message>

            <Form.Field>
              <Form.Input
                label="Email"
                name="email"
                type="email"
                placeholder="john@gmail.com"
                onChange={this.handleChange} />
            </Form.Field>

            <Form.Field>
              <Form.Input
                label="Password"
                name="password"
                type="password"
                onChange={this.handleChange} />
            </Form.Field>

            <Form.Group>
              <Form.Button primary size="huge">Sign in to your account</Form.Button>
            </Form.Group>
          </Form>
          <p>You don't have an account? <Link to="/register">Sign up here</Link></p>
        </Container>
      </Segment>
    )
  }
}

export default NotFound
