import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import { Container, Menu, Button, Image } from 'semantic-ui-react';

import logo from './images/logo.png';

import Home from './Home';
import Register from './Register';
import Login from './Login';
import ImageList from './ImageList'
import NotFound from './NotFound';

class App extends Component {
  render () {
    return(
      <Router>
        <div>
          <Menu fixed='top'>
            <Container>
              <Menu.Item as={Link} header to='/'>
                <Image size='small' src={logo} />
              </Menu.Item>

              <Menu.Item position='right'>
                <Button as={Link} basic to='/login'>
                  Log in
                </Button>
                <Button as={Link} primary style={{ marginLeft: '0.5em' }} to='/register'>
                  Sign Up
                </Button>
              </Menu.Item>
            </Container>
          </Menu>

          <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/register' exact component={Register} />
            <Route path='/login' exact component={Login} />
            <Route path='/images' exact component={ImageList} />
            <Route component={NotFound} />
          </Switch>
        </div>
      </Router>
    )
  }
}

export default App
