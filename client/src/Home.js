import React, { Component } from 'react';
import { Button, Segment, Header } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import './App.css';

class Home extends Component {
  render() {
    return (
      <Segment padded="very" textAlign="center" size="massive" vertical>
        <Header as="h1" size="huge">Welcome to Devolute Gallery</Header>
        <Header as="h3" color="grey">The only image gallery you'll ever need</Header>

        <Button primary size="massive" as={Link} to="/register" style={{ marginTop: '1em' }}>Sign up free</Button>
      </Segment>
    );
  }
}

export default Home;
