# frozen_string_literal: true

RSpec::Matchers.define :match_schema do |schema|
  match do |response|
    schema_directory = File.join(Dir.pwd, 'spec', 'support', 'schemas')
    schema_path = File.join(schema_directory, "#{schema}.schema.json")

    JSON::Validator.validate!(schema_path, response, strict: true)
  end
end
