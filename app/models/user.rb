# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  devise :database_authenticatable, :jwt_authenticatable, jwt_revocation_strategy: JWTBlacklist

  validates :full_name, presence: true

  def full_name=(name)
    names = name.split(' ')

    self.first_name = names.first
    self.last_name = names.last
  end

  def full_name
    [first_name, last_name].join(' ')
  end
end
